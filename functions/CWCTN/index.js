
var request = require('request');

// 各個 intent 實作方法
var convert = require('event/convert'); //當 intent 是 使用支出記帳工具 時，使用這支 function

exports.handle = function (e, ctx) {
    //  console.log('processing event: %j', e)
    var sentence = encodeURIComponent(e.sentence);
    // var url = 'https://api.projectoxford.ai/luis/v1/application?id=98011fe3-8929-42e7-aaac-9f08e9ccb166&subscription-key=745a8db91d24434e9e8ab970fdd56279&q=' + sentence
    // var options = {
    //     url: url,
    //     headers: {
    //         'User-Agent': 'request'
    //     }
    // };
    // request(options, callback);

    convert.main(sentence, ctx)

    // function callback(error, res, body) {

    //     if (error) {
    //         return console.log('Error:', error);
    //     }

    //     if (res.statusCode !== 200) {
    //         return console.log('Invald Status Code Returned:', res.statusCode);
    //     }

    //     convert.main(sentence, intentObj, ctx)
    // }
}
